-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 35.184.17.123
-- Generation Time: Mar 11, 2019 at 05:35 AM
-- Server version: 5.7.14-google-log
-- PHP Version: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `propiedades`
--

-- --------------------------------------------------------

--
-- Table structure for table `auth_assignment`
--

CREATE TABLE `auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `auth_assignment`
--

INSERT INTO `auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES
('admin', '2', 1552097108);

-- --------------------------------------------------------

--
-- Table structure for table `auth_item`
--

CREATE TABLE `auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` smallint(6) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` blob,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `auth_item`
--

INSERT INTO `auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES
('admin', 1, NULL, NULL, NULL, 1552097107, 1552097107);

-- --------------------------------------------------------

--
-- Table structure for table `auth_item_child`
--

CREATE TABLE `auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `auth_rule`
--

CREATE TABLE `auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` blob,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `construction`
--

CREATE TABLE `construction` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL COMMENT 'Nombre',
  `code` varchar(15) NOT NULL COMMENT 'Clave',
  `photo_1` varchar(200) DEFAULT NULL COMMENT 'Foto 1',
  `photo_2` varchar(200) DEFAULT NULL COMMENT 'Foto 2',
  `photo_3` varchar(200) DEFAULT NULL COMMENT 'Foto 3',
  `photo_4` varchar(200) DEFAULT NULL COMMENT 'Foto 4',
  `photo_5` varchar(200) DEFAULT NULL COMMENT 'Foto 5',
  `county` varchar(45) DEFAULT NULL COMMENT 'Delegación',
  `neighbor` varchar(45) DEFAULT NULL COMMENT 'Colonia',
  `street` varchar(45) DEFAULT NULL COMMENT 'Calle',
  `lat` varchar(20) DEFAULT NULL COMMENT 'Latitud',
  `long` varchar(20) DEFAULT NULL COMMENT 'Longitud',
  `created_at` varchar(45) DEFAULT NULL,
  `updated_at` varchar(45) DEFAULT NULL,
  `deleted_at` varchar(45) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Construcciones';

--
-- Dumping data for table `construction`
--

INSERT INTO `construction` (`id`, `name`, `code`, `photo_1`, `photo_2`, `photo_3`, `photo_4`, `photo_5`, `county`, `neighbor`, `street`, `lat`, `long`, `created_at`, `updated_at`, `deleted_at`, `created_by`, `updated_by`, `deleted_by`) VALUES
(1, 'Construcción 1', 'PCOM-001/19', 'https://imagenescityexpress.scdn6.secure.raxcdn.com/sites/default/files/Historia-y-movilidad_-La-Condesa-y-La-Roma.jpg', 'https://a.travel-assets.com/findyours-php/viewfinder/images/res40/168000/168207-Roma-Condesa.jpg', NULL, NULL, NULL, 'Cuauhtémoc', 'Del Valle', 'Av. Michoacán', '19.503099909953804', '-99.19763871875', '2019-03-10 20:50:32', '2019-03-10 20:50:32', NULL, 2, 2, 0),
(2, 'Construcción 2', 'PCOM-222/19', 'https://fastly.4sqi.net/img/general/2322x4128/58963029_EIm2-Jw08CSA_KcxlGQM2k4_Q3FaSZOLZc6LKR3a1co.jpg', 'https://fastly.4sqi.net/img/general/1920x1440/40012537_Z6R7okm726e_5l0HOzBg2bRNKWnjqSy43H-OcYXNckE.jpg', 'https://fastly.4sqi.net/img/general/537x720/33438145_wWd965R7Io1RS2cD2Y9jgbTEdaKiIaLh1eVaGeBC3Jo.jpg', 'https://fastly.4sqi.net/img/general/720x540/20385087_zsUT7o1-MEqYixDk1WNgGmb3LaaNLCVsQm0KIo6BFVk.jpg', 'https://fastly.4sqi.net/img/general/1440x1920/53621572_JSvOkuZdWCsDtgAlLGK1J7cFczrO0gwRGFrxFXE0FTo.jpg', 'Iztapalapa', 'Antiguo Country', 'Privada de las Gualdrapas 201', '19.4461311441095', '-99.14819952343748', '2019-03-10 21:57:34', '2019-03-10 21:57:34', NULL, 3, 3, 0),
(3, 'Construcción 3', 'PCOM-292/19', 'https://fastly.4sqi.net/img/general/537x720/9N4BE8151i4vIvNWKYu00Ryojlw542SgtE78BBso1GU.jpg', 'https://fastly.4sqi.net/img/general/960x720/35030413_kUOhrTw1Vv-WSNaxHD7MCVbxHWjJnHBgeCxPKP0TG8w.jpg', 'https://fastly.4sqi.net/img/general/612x612/5992743_tfSabyRgtnGduD0csmwg_Kl_5ZS-Du9nPN3H9PnKW7c.jpg', 'https://fastly.4sqi.net/img/general/640x640/242144_elCpPSuPwX3_BrICKDqHP4RmtZnhiUmpAuTPRmOvOWs.jpg', 'https://fastly.4sqi.net/img/general/717x959/52432366_bPSNr5NO_P898DBORGJOvL3rD_uSc6vOw9UWYc3tkLI.jpg', 'Coyoacán', 'Coyacán', '1501 India St', '19.37359767385', '-99.09876104687498', '2019-03-10 22:00:24', '2019-03-10 22:00:24', NULL, 3, 3, 0);

-- --------------------------------------------------------

--
-- Table structure for table `feature`
--

CREATE TABLE `feature` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL COMMENT 'Nombre',
  `value` varchar(200) NOT NULL COMMENT 'Detalle',
  `construction_id` int(11) NOT NULL COMMENT 'Construcción',
  `created_at` varchar(45) DEFAULT NULL,
  `updated_at` varchar(45) DEFAULT NULL,
  `deleted_at` varchar(45) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Características';

--
-- Dumping data for table `feature`
--

INSERT INTO `feature` (`id`, `name`, `value`, `construction_id`, `created_at`, `updated_at`, `deleted_at`, `created_by`, `updated_by`, `deleted_by`) VALUES
(1, 'Metros de Terreno', '200m', 1, '2019-03-10 20:50:32', '2019-03-10 20:50:32', NULL, 2, 2, 0),
(2, 'Casa', 'Habitación', 3, '2019-03-10 22:00:24', '2019-03-10 22:00:24', NULL, 3, 3, 0),
(3, 'Amenidades', 'Alberca', 3, '2019-03-10 22:00:24', '2019-03-10 22:00:24', NULL, 3, 3, 0);

-- --------------------------------------------------------

--
-- Table structure for table `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('Da\\User\\Migration\\m000000_000001_create_user_table', 1552095799),
('Da\\User\\Migration\\m000000_000002_create_profile_table', 1552095800),
('Da\\User\\Migration\\m000000_000003_create_social_account_table', 1552095801),
('Da\\User\\Migration\\m000000_000004_create_token_table', 1552095801),
('Da\\User\\Migration\\m000000_000005_add_last_login_at', 1552095802),
('Da\\User\\Migration\\m000000_000006_add_two_factor_fields', 1552095803),
('Da\\User\\Migration\\m000000_000007_enable_password_expiration', 1552095803),
('m000000_000000_base', 1552091248),
('m140506_102106_rbac_init', 1552095970),
('m170907_052038_rbac_add_index_on_auth_assignment_user_id', 1552095971),
('m180523_151638_rbac_updates_indexes_without_prefix', 1552095971);

-- --------------------------------------------------------

--
-- Table structure for table `profile`
--

CREATE TABLE `profile` (
  `user_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `public_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gravatar_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gravatar_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `timezone` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bio` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `profile`
--

INSERT INTO `profile` (`user_id`, `name`, `public_email`, `gravatar_email`, `gravatar_id`, `location`, `website`, `timezone`, `bio`) VALUES
(2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `social_account`
--

CREATE TABLE `social_account` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `client_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `token`
--

CREATE TABLE `token` (
  `user_id` int(11) DEFAULT NULL,
  `code` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `type` smallint(6) NOT NULL,
  `created_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `unconfirmed_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `registration_ip` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `flags` int(11) NOT NULL DEFAULT '0',
  `confirmed_at` int(11) DEFAULT NULL,
  `blocked_at` int(11) DEFAULT NULL,
  `updated_at` int(11) NOT NULL,
  `created_at` int(11) NOT NULL,
  `last_login_at` int(11) DEFAULT NULL,
  `auth_tf_key` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  `auth_tf_enabled` tinyint(1) DEFAULT '0',
  `password_changed_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `email`, `password_hash`, `auth_key`, `unconfirmed_email`, `registration_ip`, `flags`, `confirmed_at`, `blocked_at`, `updated_at`, `created_at`, `last_login_at`, `auth_tf_key`, `auth_tf_enabled`, `password_changed_at`) VALUES
(2, 'admin', 'luisarmando1234@gmail.com', '$2y$10$/etqDG2qt/zDMoa9OtKHvOoLg5BB4JcnABGdnCQuLbU6GL.8wrKTe', 'B4V6lQ3k0tOMl6vGNjM4_xiJCX5znSIN', NULL, NULL, 0, 1552097107, NULL, 1552097107, 1552097107, 1552271212, '', 0, 1552097107),
(3, 'contact', 'contact@inquid.co', '$2y$10$Afhxv5YKr3Hqu7j4vhXkaOF9TvBDHH180jHG1yRbtuMNIJKg5SzXG', '80ghgx7F3q4cIxgbFcouRsXQARrQ6wv1', NULL, '127.0.0.1', 0, 1552271310, NULL, 1552271310, 1552271310, 1552271323, '', 0, 1552271310);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD PRIMARY KEY (`item_name`,`user_id`),
  ADD KEY `idx-auth_assignment-user_id` (`user_id`);

--
-- Indexes for table `auth_item`
--
ALTER TABLE `auth_item`
  ADD PRIMARY KEY (`name`),
  ADD KEY `rule_name` (`rule_name`),
  ADD KEY `idx-auth_item-type` (`type`);

--
-- Indexes for table `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD PRIMARY KEY (`parent`,`child`),
  ADD KEY `child` (`child`);

--
-- Indexes for table `auth_rule`
--
ALTER TABLE `auth_rule`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `construction`
--
ALTER TABLE `construction`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `feature`
--
ALTER TABLE `feature`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_feature_construction_idx` (`construction_id`);

--
-- Indexes for table `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `profile`
--
ALTER TABLE `profile`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `social_account`
--
ALTER TABLE `social_account`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `idx_social_account_provider_client_id` (`provider`,`client_id`),
  ADD UNIQUE KEY `idx_social_account_code` (`code`),
  ADD KEY `fk_social_account_user` (`user_id`);

--
-- Indexes for table `token`
--
ALTER TABLE `token`
  ADD UNIQUE KEY `idx_token_user_id_code_type` (`user_id`,`code`,`type`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `idx_user_username` (`username`),
  ADD UNIQUE KEY `idx_user_email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `construction`
--
ALTER TABLE `construction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `feature`
--
ALTER TABLE `feature`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `profile`
--
ALTER TABLE `profile`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `social_account`
--
ALTER TABLE `social_account`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `auth_item`
--
ALTER TABLE `auth_item`
  ADD CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `feature`
--
ALTER TABLE `feature`
  ADD CONSTRAINT `fk_feature_construction` FOREIGN KEY (`construction_id`) REFERENCES `construction` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `profile`
--
ALTER TABLE `profile`
  ADD CONSTRAINT `fk_profile_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `social_account`
--
ALTER TABLE `social_account`
  ADD CONSTRAINT `fk_social_account_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `token`
--
ALTER TABLE `token`
  ADD CONSTRAINT `fk_token_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
