<?php

namespace app\modules\construcciones\controllers;

use yii\web\Controller;

/**
 * Default controller for the `construcciones` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->redirect('/construcciones/construction/index');
    }
}
