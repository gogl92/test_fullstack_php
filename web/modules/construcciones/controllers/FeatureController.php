<?php

namespace app\modules\construcciones\controllers;

use app\modules\construcciones\components\FeaturePDF;
use app\modules\construcciones\models\Feature;
use app\modules\construcciones\models\search\FeatureSearch;
use Exception;
use inquid\yiireports\ExcelHelper;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Yii;
use yii\base\InvalidConfigException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * FeatureController implements the CRUD actions for Feature model.
 */
class FeatureController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => [
                            'index',
                            'view',
                            'create',
                            'update',
                            'delete',
                            'pdf',
                            'import',
                            'import-validate',
                            'import-excel',
                            'get-format'
                        ],
                        'roles' => ['@']
                    ],
                    [
                        'allow' => false
                    ]
                ]
            ]
        ];
    }

    /**
     * Lists all Feature models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FeatureSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Feature model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Feature model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws \yii\db\Exception
     */
    public function actionCreate()
    {
        $model = new Feature();
        if ($model->loadAll(Yii::$app->request->post())) {
            if ($model->saveAll()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
            return $this->render('create', [
                'model' => $model,
            ]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Feature model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \yii\db\Exception
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Feature model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \yii\db\Exception
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->deleteWithRelated();

        return $this->redirect(['index']);
    }

    /**
     * Export Feature information into PDF format.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionPdf($id)
    {
        $model = $this->findModel($id);
        $voucher = new FeaturePDF('P', 'mm', 'Letter');
        $voucher->Feature = $model;
        $voucher->Body();
        $voucher->Output('I', "Feature-{$id}.pdf", true);
        exit;
    }


    /**
     * Finds the Feature model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Feature the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Feature::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('La página solicitada no existe.');
        }
    }

    /* Excel Zone */
    /**
     * @param int $id
     * @return bool|\yii\web\Response
     */
    public function actionGetFormat($format = false)
    {
        $excel = new ExcelHelper();
        try {
            $data = Feature::find()
                ->select([
                    'id',
                    'name',
                    'value',
                    'construction_id',
                ]);
            if ($format) {
                $data->where(['id' => -1]);
            }
            $excel->createExportTable(
                $data->asArray()->all(),
                [
                    ['coordinate' => 'A1', 'title' => 'id'],
                    ['coordinate' => 'B1', 'title' => 'name'],
                    ['coordinate' => 'C1', 'title' => 'value'],
                    ['coordinate' => 'D1', 'title' => 'construction_id'],
                ]);
            $excel->autoSizeColumns([
                'A',
                'B',
                'C',
                'D',
            ]);
            return $this->redirect($excel->saveExcel('files/formats', 'FormatoImportarFeature'));
        } catch (Exception $e) {
            return false;
        } catch (InvalidConfigException $e) {
            return false;
        }
    }

    /**
     * @return string
     */
    public function actionImport()
    {
        return $this->render('import');
    }

    /**
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @throws \Exception
     */
    public function actionImportValidate()
    {
        $personal = new Feature();
        $personal->fileExcelImport = UploadedFile::getInstanceByName('fileExcelTest');
        $personal->fileExcelImport->saveAs('files/Feature/tmp_' . $personal->fileExcelImport->baseName . '_' . $personal->id . $personal->fileExcelImport->extension);
        $path = './files/Feature/tmp_' . $personal->fileExcelImport->baseName . '_' . $personal->id . $personal->fileExcelImport->extension;
        $inputFileType = IOFactory::identify($path);
        $reader = IOFactory::createReader($inputFileType);
        $spreadsheet = $reader->load($path);
        $highestRow = $spreadsheet->getActiveSheet()->getHighestRow();
        $data = $spreadsheet->getActiveSheet()->rangeToArray('A1:U' . $highestRow, null, true, false);
        if ($this->extractData($data, true)) {
            Yii::$app->session->setFlash('success', 'La información es correcta y se puede subir al sistema');
        } else {
            Yii::$app->session->setFlash('error', 'La información contiene errores favor de revisar');
        }
    }

    /**
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @throws \Exception
     */
    public function actionImportExcel()
    {
        $personal = new Feature();
        $personal->fileExcelImport = UploadedFile::getInstanceByName('fileExcel');
        $personal->fileExcelImport->saveAs('files/Feature/' . $personal->fileExcelImport->baseName . '_' . $personal->id . $personal->fileExcelImport->extension);
        $path = './files/Feature/' . $personal->fileExcelImport->baseName . '_' . $personal->id . $personal->fileExcelImport->extension;
        $inputFileType = IOFactory::identify($path);
        $reader = IOFactory::createReader($inputFileType);
        $spreadsheet = $reader->load($path);
        $highestRow = $spreadsheet->getActiveSheet()->getHighestRow();
        $data = $spreadsheet->getActiveSheet()->rangeToArray('A1:U' . $highestRow, null, true, false);
        if ($this->extractData($data)) {
            Yii::$app->session->setFlash('success', 'La información fue ingresada al sistema de forma correcta');
        } else {
            Yii::$app->session->setFlash('error', 'La información contiene errores favor de revisar');
        }
    }

    /**
     * @param $data
     * @param bool $test
     * @return array|bool
     */
    private function extractData($data, $test = false)
    {
        Yii::debug('Data to import to movements' . Json::encode($data));
        unset($data[0]);
        foreach ($data as $datum) {
            $personal = Feature::find()->where(['id' => (int)$datum[0]])->one();
            if ($personal === null) {
                $personal = new Feature();
            }
            $personal->id = (string)$datum[0];
            $personal->name = (string)$datum[1];
            $personal->value = (string)$datum[2];
            $personal->construction_id = (string)$datum[3];
            if ($test) {
                if (!$personal->validate()) {
                    Yii::debug('Errors' . Json::encode($personal->getErrors()));
                    return false;
                }
                return true;
            }
            if (!$personal->save()) {
                Yii::debug($personal->getErrors());
                return false;
            }
        }
        return true;
    }
    //END EXCEL Zone
}
