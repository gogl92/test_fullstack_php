<?php

namespace app\modules\construcciones\controllers;

use app\modules\construcciones\components\ConstructionPDF;
use app\modules\construcciones\models\Construction;
use app\modules\construcciones\models\search\ConstructionSearch;
use Exception;
use inquid\yiireports\ExcelHelper;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Yii;
use yii\base\InvalidConfigException;
use yii\data\ArrayDataProvider;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * ConstructionController implements the CRUD actions for Construction model.
 */
class ConstructionController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => [
                            'index',
                            'view',
                            'create',
                            'update',
                            'delete',
                            'pdf',
                            'import',
                            'import-validate',
                            'import-excel',
                            'get-format',
                            'add-feature',
                            'map'
                        ],
                        'roles' => ['@']
                    ],
                    [
                        'allow' => false
                    ]
                ]
            ]
        ];
    }

    /**
     * Lists all Construction models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ConstructionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Construction model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $providerFeature = new ArrayDataProvider([
            'allModels' => $model->features,
        ]);
        return $this->render('view', [
            'model' => $this->findModel($id),
            'providerFeature' => $providerFeature,
        ]);
    }

    /**
     * Creates a new Construction model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws \yii\db\Exception
     */
    public function actionCreate()
    {
        $model = new Construction();
        if ($model->loadAll(Yii::$app->request->post())) {
            if ($model->saveAll()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
            return $this->render('create', [
                'model' => $model,
            ]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Construction model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \yii\db\Exception
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Construction model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \yii\db\Exception
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->deleteWithRelated();

        return $this->redirect(['index']);
    }

    /**
     * Export Construction information into PDF format.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionPdf($id)
    {
        $model = $this->findModel($id);
        $providerFeature = new ArrayDataProvider([
            'allModels' => $model->features,
        ]);
        $voucher = new ConstructionPDF('P', 'mm', 'Letter');
        $voucher->Construction = $model;
        $voucher->Body();
        $voucher->Output('I', "Construction-{$id}.pdf", true);
        exit;
    }


    /**
     * Finds the Construction model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Construction the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Construction::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('La página solicitada no existe.');
        }
    }

    /**
     * Action to load a tabular form grid
     * for Feature
     * @author Yohanes Candrajaya <moo.tensai@gmail.com>
     * @author Jiwantoro Ndaru <jiwanndaru@gmail.com>
     *
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionAddFeature()
    {
        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('Feature');
            if ((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
                $row[] = [];
            return $this->renderAjax('_formFeature', ['row' => $row]);
        } else {
            throw new NotFoundHttpException('La página solicitada no existe.');
        }
    }

    /* Excel Zone */
    /**
     * @param int $id
     * @return bool|\yii\web\Response
     */
    public function actionGetFormat($format = false)
    {
        $excel = new ExcelHelper();
        try {
            $data = Construction::find()
                ->select([
                    'id',
                    'name',
                    'code',
                    'photo_1',
                    'photo_2',
                    'photo_3',
                    'photo_4',
                    'photo_5',
                    'county',
                    'neighbor',
                    'street',
                    'lat',
                    'long',
                ]);
            if ($format) {
                $data->where(['id' => -1]);
            }
            $excel->createExportTable(
                $data->asArray()->all(),
                [
                    ['coordinate' => 'A1', 'title' => 'id'],
                    ['coordinate' => 'B1', 'title' => 'name'],
                    ['coordinate' => 'C1', 'title' => 'code'],
                    ['coordinate' => 'D1', 'title' => 'photo_1'],
                    ['coordinate' => 'E1', 'title' => 'photo_2'],
                    ['coordinate' => 'F1', 'title' => 'photo_3'],
                    ['coordinate' => 'G1', 'title' => 'photo_4'],
                    ['coordinate' => 'H1', 'title' => 'photo_5'],
                    ['coordinate' => 'I1', 'title' => 'county'],
                    ['coordinate' => 'J1', 'title' => 'neighbor'],
                    ['coordinate' => 'K1', 'title' => 'street'],
                    ['coordinate' => 'L1', 'title' => 'lat'],
                    ['coordinate' => 'M1', 'title' => 'long'],
                ]);
            $excel->autoSizeColumns([
                'A',
                'B',
                'C',
                'D',
                'E',
                'F',
                'G',
                'H',
                'I',
                'J',
                'K',
                'L',
                'M',
            ]);
            return $this->redirect($excel->saveExcel('files/formats', 'FormatoImportarConstruction'));
        } catch (Exception $e) {
            return false;
        } catch (InvalidConfigException $e) {
            return false;
        }
    }

    /**
     * @return string
     */
    public function actionImport()
    {
        return $this->render('import');
    }

    /**
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @throws \Exception
     */
    public function actionImportValidate()
    {
        $personal = new Construction();
        $personal->fileExcelImport = UploadedFile::getInstanceByName('fileExcelTest');
        $personal->fileExcelImport->saveAs('files/Construction/tmp_' . $personal->fileExcelImport->baseName . '_' . $personal->id . $personal->fileExcelImport->extension);
        $path = './files/Construction/tmp_' . $personal->fileExcelImport->baseName . '_' . $personal->id . $personal->fileExcelImport->extension;
        $inputFileType = IOFactory::identify($path);
        $reader = IOFactory::createReader($inputFileType);
        $spreadsheet = $reader->load($path);
        $highestRow = $spreadsheet->getActiveSheet()->getHighestRow();
        $data = $spreadsheet->getActiveSheet()->rangeToArray('A1:U' . $highestRow, null, true, false);
        if ($this->extractData($data, true)) {
            Yii::$app->session->setFlash('success', 'La información es correcta y se puede subir al sistema');
        } else {
            Yii::$app->session->setFlash('error', 'La información contiene errores favor de revisar');
        }
    }

    /**
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @throws \Exception
     */
    public function actionImportExcel()
    {
        $personal = new Construction();
        $personal->fileExcelImport = UploadedFile::getInstanceByName('fileExcel');
        $personal->fileExcelImport->saveAs('files/Construction/' . $personal->fileExcelImport->baseName . '_' . $personal->id . $personal->fileExcelImport->extension);
        $path = './files/Construction/' . $personal->fileExcelImport->baseName . '_' . $personal->id . $personal->fileExcelImport->extension;
        $inputFileType = IOFactory::identify($path);
        $reader = IOFactory::createReader($inputFileType);
        $spreadsheet = $reader->load($path);
        $highestRow = $spreadsheet->getActiveSheet()->getHighestRow();
        $data = $spreadsheet->getActiveSheet()->rangeToArray('A1:U' . $highestRow, null, true, false);
        if ($this->extractData($data)) {
            Yii::$app->session->setFlash('success', 'La información fue ingresada al sistema de forma correcta');
        } else {
            Yii::$app->session->setFlash('error', 'La información contiene errores favor de revisar');
        }
    }

    /**
     * @param $data
     * @param bool $test
     * @return array|bool
     */
    private function extractData($data, $test = false)
    {
        Yii::debug('Data to import to movements' . Json::encode($data));
        unset($data[0]);
        foreach ($data as $datum) {
            $personal = Construction::find()->where(['id' => (int)$datum[0]])->one();
            if ($personal === null) {
                $personal = new Construction();
            }
            $personal->id = (string)$datum[0];
            $personal->name = (string)$datum[1];
            $personal->code = (string)$datum[2];
            $personal->photo_1 = (string)$datum[3];
            $personal->photo_2 = (string)$datum[4];
            $personal->photo_3 = (string)$datum[5];
            $personal->photo_4 = (string)$datum[6];
            $personal->photo_5 = (string)$datum[7];
            $personal->county = (string)$datum[8];
            $personal->neighbor = (string)$datum[9];
            $personal->street = (string)$datum[10];
            $personal->lat = (string)$datum[11];
            $personal->long = (string)$datum[12];
            if ($test) {
                if (!$personal->validate()) {
                    Yii::debug('Errors' . Json::encode($personal->getErrors()));
                    return false;
                }
                return true;
            }
            if (!$personal->save()) {
                Yii::debug($personal->getErrors());
                return false;
            }
        }
        return true;
    }

    //END EXCEL Zone

    public function actionMap()
    {
        return $this->render('map', ['data' => Construction::find()->all()]);
    }
}
