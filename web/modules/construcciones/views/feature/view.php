<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\construcciones\models\Feature */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Feature', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="feature-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Característica' . ' ' . Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
            <?=
            Html::a('<i class="fa glyphicon glyphicon-hand-up"></i> ' . 'PDF',
                ['pdf', 'id' => $model->id],
                [
                    'class' => 'btn btn-danger',
                    'target' => '_blank',
                    'data-toggle' => 'tooltip',
                    'title' => 'Abrir PDF en una nueva ventana'
                ]
            ) ?>

            <?= Html::a('Actualizar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Eliminar', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => '¿Desea Eliminar este elemento?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <?php
            $gridColumn = [
                ['attribute' => 'id', 'visible' => false],
                'name',
                'value',
                [
                    'attribute' => 'construction.name',
                    'label' => 'Construction',
                ],
            ];
            echo DetailView::widget([
                'model' => $model,
                'attributes' => $gridColumn
            ]);
            ?>
        </div>
    </div>
    <div class="row">
        <h4>Construction<?= ' ' . Html::encode($this->title) ?></h4>
    </div>
    <?php
    $gridColumnConstruction = [
        ['attribute' => 'id', 'visible' => false],
        'name',
        'code',
        'photo_1',
        'photo_2',
        'photo_3',
        'photo_4',
        'photo_5',
        'county',
        'neighbor',
        'street',
        'lat',
        'long',
    ];
    echo DetailView::widget([
        'model' => $model->construction,
        'attributes' => $gridColumnConstruction]);
    ?>
</div>
