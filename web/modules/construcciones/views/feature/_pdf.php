<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\construcciones\models\Feature */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Característica', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="feature-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Feature' . ' ' . Html::encode($this->title) ?></h2>
        </div>
    </div>

    <div class="row">
        <?php
        $gridColumn = [
            ['attribute' => 'id', 'visible' => false],
            'name',
            'value',
            [
                'attribute' => 'construction.name',
                'label' => 'Construction'
            ],
        ];
        echo DetailView::widget([
            'model' => $model,
            'attributes' => $gridColumn
        ]);
        ?>
    </div>
</div>
