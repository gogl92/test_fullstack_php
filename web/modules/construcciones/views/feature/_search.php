<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\construcciones\models\search\FeatureSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-feature-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'v-model' => 'name']) ?>

    <?= $form->field($model, 'value')->textInput(['maxlength' => true, 'v-model' => 'value']) ?>

    <?= $form->field($model, 'construction_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\modules\construcciones\models\Construction::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
        'options' => [],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <div class="form-group">
        <?= Html::submitButton('Buscar', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Borrar Busqueda', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
