<?php

use inquid\vue\Vue;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\construcciones\models\Feature */
/* @var $form yii\widgets\ActiveForm */

Vue::begin([
    'jsName' => 'feature',
    'id' => "feature-form",
    'data' => [
        'id' => isset($model->id) ? $model->id : '',
        'name' => isset($model->name) ? $model->name : '',
        'value' => isset($model->value) ? $model->value : '',
        'construction_id' => isset($model->construction_id) ? $model->construction_id : '',
    ],
    'methods' => [
        'reverseMessage' => new yii\web\JsExpression("function(){"
            . "this.message =1; "
            . "}"),
    ]
]);
?>
<div class="feature-form">

    <?php $form = ActiveForm::begin(['options' => ['class' => 'disable-submit-buttons']]); ?>

    <?= $form->errorSummary($model); ?>

    <div class='row'></div>
    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <div class='row'>
        <div class='col-md-3'>
            <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'v-model' => 'name']) ?>

        </div>
        <div class='col-md-3'>
            <?= $form->field($model, 'value')->textInput(['maxlength' => true, 'v-model' => 'value']) ?>

        </div>
        <div class='col-md-3'>
            <?= $form->field($model, 'construction_id')->widget(\kartik\widgets\Select2::classname(), [
                'data' => \yii\helpers\ArrayHelper::map(\app\modules\construcciones\models\Construction::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
                'options' => [],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]); ?>

        </div>
    </div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Agregar' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary', 'data' => ['disabled-text' => 'Please Wait']]) ?>
        <?= Html::a(Yii::t('app', 'Cancelar'), Yii::$app->request->referrer, ['class' => 'btn btn-danger']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    <?php Vue::end(); ?>
</div>
