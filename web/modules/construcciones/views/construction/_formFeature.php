<div class="form-group" id="add-feature">
    <?php

    use kartik\builder\TabularForm;
    use kartik\grid\GridView;
    use yii\data\ArrayDataProvider;
    use yii\helpers\Html;

    $dataProvider = new ArrayDataProvider([
        'allModels' => $row,
        'pagination' => [
            'pageSize' => -1
        ]
    ]);
    echo TabularForm::widget([
        'dataProvider' => $dataProvider,
        'formName' => 'Feature',
        'checkboxColumn' => false,
        'actionColumn' => false,
        'attributeDefaults' => [
            'type' => TabularForm::INPUT_TEXT,
        ],
        'attributes' => [
            "id" => ['type' => TabularForm::INPUT_HIDDEN, 'columnOptions' => ['hidden' => true]],
            'name' => ['type' => TabularForm::INPUT_TEXT,'label'=>'Característica'],
            'value' => ['type' => TabularForm::INPUT_TEXT,'label'=>'Valor'],
            'del' => [
                'type' => 'raw',
                'label' => '',
                'value' => function ($model, $key) {
                    return
                        Html::hiddenInput('Children[' . $key . '][id]', (!empty($model['id'])) ? $model['id'] : "") .
                        Html::a('<i class="glyphicon glyphicon-trash"></i>', '#', ['title' => 'Eliminar', 'onClick' => 'delRowFeature(' . $key . '); return false;', 'id' => 'feature-del-btn']);
                },
            ],
        ],
        'gridSettings' => [
            'panel' => [
                'heading' => false,
                'type' => GridView::TYPE_DEFAULT,
                'before' => false,
                'footer' => false,
                'after' => Html::button('<i class="glyphicon glyphicon-plus"></i>' . 'Agregar Característica', ['type' => 'button', 'class' => 'btn btn-success kv-batch-create', 'onClick' => 'addRowFeature()']),
            ]
        ]
    ]);
    echo "    </div>\n\n";
    ?>
