<?php

/* @var $this yii\web\View */
/* @var $searchModel app\modules\construcciones\models\search\ConstructionSearch */

/* @var $dataProvider yii\data\ActiveDataProvider */

use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\helpers\Html;

$this->title = 'Construcciones';
$this->params['breadcrumbs'][] = $this->title;
$search = "$('.search-button').click(function(){
	$('.search-form').toggle(1000);
	return false;
});";
$this->registerJs($search);
?>
<div class="construction-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php //$this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Agregar Construcción', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Busqueda Avanzada', '#', ['class' => 'btn btn-info search-button']) ?>
        <?= Html::a('Importar desde Excel ', ['import'], ['class' => 'btn btn-danger']) ?>
        <?= Html::a('Vista General En Mapa ', ['map'], ['class' => 'btn btn-danger']) ?>
    </p>
    <div class="search-form" style="display:none">
        <?= $this->render('_search', ['model' => $searchModel]); ?>
    </div>
    <?php
    $gridColumn = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        'name',
        'code',
        'street',
        'neighbor',
        'county',
        [
            'class' => 'kartik\grid\ActionColumn',
        ],
    ];
    ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumn,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-construction']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span>  ' . Html::encode($this->title),
        ],
        // your toolbar can include the additional full export menu
        'toolbar' => [
            '{export}',
            ExportMenu::widget([
                'filename' => 'construction',
                'dataProvider' => $dataProvider,
                'columns' => $gridColumn,
                'target' => ExportMenu::TARGET_BLANK,
                'fontAwesome' => true,
                'dropdownOptions' => [
                    'label' => 'Full',
                    'class' => 'btn btn-default',
                    'itemsBefore' => [
                        '<li class="dropdown-header">Exportar Información</li>',
                    ],
                ],
            ]),
        ],
    ]); ?>

</div>
