<?php

use dosamigos\google\maps\LatLng;
use dosamigos\google\maps\Map;
use dosamigos\google\maps\overlays\InfoWindow;
use dosamigos\google\maps\overlays\Marker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\construcciones\models\Construction */
/* @var $form yii\widgets\ActiveForm */

\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos' => \yii\web\View::POS_END,
    'viewParams' => [
        'class' => 'Feature',
        'relID' => 'feature',
        'value' => \yii\helpers\Json::encode($model->features),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);

$script = <<< SCRIPT
function (chrs, buffer, pos, strict, opts) {
   var valExp2 = new RegExp("([PCOM-XXX/19])\w+");
   return valExp2.test(buffer[pos - 1] + chrs);
}
SCRIPT;
?>
<div class="construction-form">

    <?php $form = ActiveForm::begin(['options' => ['class' => 'disable-submit-buttons']]); ?>

    <?= $form->errorSummary($model); ?>

    <div class='row'></div>
    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <div class='row'>
        <div class='col-md-3'>
            <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'v-model' => 'name']) ?>

        </div>
        <div class='col-md-3'>
            <?= $form->field($model, 'code')->widget(\yii\widgets\MaskedInput::className(), [
                'clientOptions' => [
                    'alias' => 'string',
                    'prefix'=>'PCOM-',
                    'suffix'=>'/',
                    'mask' => 'PCOM-[9][9][9]/[9][9]'
                ],
                'value' => 'PCOM-123/19'
            ]) ?>
        </div>
        <div class='col-md-3'>
            <?= $form->field($model, 'street')->textInput(['maxlength' => true, 'v-model' => 'street']) ?>

        </div>
        <div class='col-md-3'>
            <?= $form->field($model, 'neighbor')->textInput(['maxlength' => true, 'v-model' => 'neighbor']) ?>

        </div>
    </div>
    <div class="row">
        <div class='col-md-3'>
            <?= $form->field($model, 'county')->textInput(['maxlength' => true, 'v-model' => 'county']) ?>

        </div>
        <div class='col-md-3'>
            <?= $form->field($model, 'lat')->textInput(['maxlength' => true, 'placeholder' => 'Latitud']) ?>

        </div>
        <div class='col-md-3'>
            <?= $form->field($model, 'long')->textInput(['maxlength' => true, 'placeholder' => 'Longitud']) ?>

        </div>
        <div class='col-md-3'>

        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <h3>Arrastre el punto hasta la ubicación de la construcción</h3>
            <?php
            if (isset($model->lat, $model->long)) {
                $coord = new LatLng(['lat' => $model->lat, 'lng' => $model->long]);
            } else {
                $coord = new LatLng(['lat' => 19.503099, 'lng' => -99.197638]);
            }
            $map = new Map([
                'center' => $coord,
                'zoom' => 8,
                'width' => 'auto'
            ]);
            // Lets add a marker now
            $marker = new Marker([
                'position' => $coord,
                'draggable' => true,
                'title' => 'Coto',
            ]);
            // Provide a shared InfoWindow to the marker
            $marker->attachInfoWindow(
                new InfoWindow([
                    'content' => '<p>Construcción</p>'
                ])
            );
            // Add marker to the map
            $map->addOverlay($marker);
            $marker->addEvent(new \dosamigos\google\maps\Event([
                'trigger' => 'dragend',
                'js' => '$("#construction-lat").val(gmarker1.getPosition()["lat"]); $("#construction-long").val(gmarker1.getPosition()["lng"]);'
            ]));
            echo $map->display();
            ?>
            <br>

        </div>
    </div> <?php
    $forms = [
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Características'),
            'content' => $this->render('_formFeature', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->features),
            ]),
        ],
    ];
    echo kartik\tabs\TabsX::widget([
        'items' => $forms,
        'position' => kartik\tabs\TabsX::POS_ABOVE,
        'encodeLabels' => false,
        'pluginOptions' => [
            'bordered' => true,
            'sideways' => true,
            'enableCache' => false,
        ],
    ]);
    ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Agregar' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary', 'data' => ['disabled-text' => 'Please Wait']]) ?>
        <?= Html::a(Yii::t('app', 'Cancelar'), Yii::$app->request->referrer, ['class' => 'btn btn-danger']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
