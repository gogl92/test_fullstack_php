<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\construcciones\models\Construction */

$this->title = 'Agregar Construcciones';
$this->params['breadcrumbs'][] = ['label' => 'Construcciones', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="construction-create">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
        </div>
            <div class="box-body">
                <?= $this->render('_form', [
                    'model' => $model,
                ]) ?>
        </div>
    </div>
</div>
