<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\modules\construcciones\models\Construction */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Construction', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="construction-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Construction'.' '. Html::encode($this->title) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'name',
        'code',
        'photo_1',
        'photo_2',
        'photo_3',
        'photo_4',
        'photo_5',
        'county',
        'neighbor',
        'street',
        'lat',
        'long',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>

    <div class="row">
<?php
if($providerFeature->totalCount){
    $gridColumnFeature = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        'name',
        'value',
            ];
    echo Gridview::widget([
        'dataProvider' => $providerFeature,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => Html::encode('Feature'),
        ],
        'panelHeadingTemplate' => '<h4>{heading}</h4>{summary}',
        'toggleData' => false,
        'columns' => $gridColumnFeature
    ]);
}
?>
    </div>
</div>
