<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\construcciones\models\search\ConstructionSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-construction-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'v-model'=>'name']) ?>

    <?= $form->field($model, 'code')->textInput(['maxlength' => true, 'v-model'=>'code']) ?>

    <?= $form->field($model, 'photo_1')->textInput(['maxlength' => true, 'v-model'=>'photo_1']) ?>

    <?= $form->field($model, 'photo_2')->textInput(['maxlength' => true, 'v-model'=>'photo_2']) ?>

    <?php /* echo $form->field($model, 'photo_3')->textInput(['maxlength' => true, 'v-model'=>'photo_3']) */ ?>

    <?php /* echo $form->field($model, 'photo_4')->textInput(['maxlength' => true, 'v-model'=>'photo_4']) */ ?>

    <?php /* echo $form->field($model, 'photo_5')->textInput(['maxlength' => true, 'v-model'=>'photo_5']) */ ?>

    <?php /* echo $form->field($model, 'county')->textInput(['maxlength' => true, 'v-model'=>'county']) */ ?>

    <?php /* echo $form->field($model, 'neighbor')->textInput(['maxlength' => true, 'v-model'=>'neighbor']) */ ?>

    <?php /* echo $form->field($model, 'street')->textInput(['maxlength' => true, 'v-model'=>'street']) */ ?>

    <?php /* echo $form->field($model, 'lat')->textInput(['maxlength' => true, 'v-model'=>'lat']) */ ?>

    <?php /* echo $form->field($model, 'long')->textInput(['maxlength' => true, 'v-model'=>'long']) */ ?>

    <div class="form-group">
        <?= Html::submitButton('Buscar', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Borrar Busqueda', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
