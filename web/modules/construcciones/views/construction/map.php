<?php

/**
 * Created by PhpStorm.
 * User: gogl92
 * Date: 2019-03-09
 * Time: 00:09
 */

use dosamigos\google\maps\LatLng;
use dosamigos\google\maps\Map;
use dosamigos\google\maps\overlays\InfoWindow;
use dosamigos\google\maps\overlays\Marker;

/* @var $this \yii\web\View */
/* @var $data \app\modules\construcciones\models\ActiveQuery\ConstructionQuery[]|\app\modules\construcciones\models\Construction[]|array */
$this->registerCss('img{max-width: 100% !important;}')
?>
<div class="row">
    <div class="col-sm-12">
        <h1>Mapa de construcciones</h1>
    </div>
</div>
<div class="row">
    <div class="col-sm-12"><?php
        if ($data != null && count($data) != 0) {
            $coord = new LatLng(['lat' => $data[0]->lat, 'lng' => $data[0]->long]);
            $map = new Map([
                'center' => $coord,
                'zoom' => 8,
                'width' => 'auto'
            ]);
            foreach ($data as $model) {
                $coord = new LatLng(['lat' => $model->lat, 'lng' => $model->long]);
                $marker = new Marker([
                    'position' => $coord,
                    'draggable' => false,
                    'title' => $this->title,
                ]);
                $items = [
                    [
                        'url' => $model->photo_1 ?: Yii::$app->params['not_found_image_url'],
                        'src' => $model->photo_1 ?: Yii::$app->params['not_found_image_url'],
                        'options' => ['title' => $model->name]
                    ],
                    [
                        'url' => $model->photo_2 ?: Yii::$app->params['not_found_image_url'],
                        'src' => $model->photo_2 ?: Yii::$app->params['not_found_image_url'],
                        'options' => ['title' => $model->name]
                    ],
                    [
                        'url' => $model->photo_3 ?: Yii::$app->params['not_found_image_url'],
                        'src' => $model->photo_3 ?: Yii::$app->params['not_found_image_url'],
                        'options' => ['title' => $model->name]
                    ],
                    [
                        'url' => $model->photo_4 ?: Yii::$app->params['not_found_image_url'],
                        'src' => $model->photo_4 ?: Yii::$app->params['not_found_image_url'],
                        'options' => ['title' => $model->name]
                    ],
                    [
                        'url' => $model->photo_5 ?: Yii::$app->params['not_found_image_url'],
                        'src' => $model->photo_5 ?: Yii::$app->params['not_found_image_url'],
                        'options' => ['title' => $model->name]
                    ]
                ];
                $marker->attachInfoWindow(
                    new InfoWindow([
                        'content' => "<div class='row'><div class='col-sm-12'>
                                        <p>{$model->name}</p>
                                        " . dosamigos\gallery\Gallery::widget(['items' => $items]) . "</br><a href='view?id={$model->id}'>Detalles</a>
</div> </div>"
                    ])
                );
                $map->addOverlay($marker);
            }
            echo $map->display();
        } else {
            echo '<h1>No Hay Información que mostrar</h1>';
        } ?>
    </div>
</div>
