<?php

use dosamigos\google\maps\LatLng;
use dosamigos\google\maps\Map;
use dosamigos\google\maps\overlays\Marker;
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\construcciones\models\Construction */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Construcción', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="construction-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Detalles de' . ' ' . Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
            <?=
            Html::a('<i class="fa glyphicon glyphicon-hand-up"></i> ' . 'PDF',
                ['pdf', 'id' => $model->id],
                [
                    'class' => 'btn btn-danger',
                    'target' => '_blank',
                    'data-toggle' => 'tooltip',
                    'title' => 'Abrir PDF en una nueva ventana'
                ]
            ) ?>

            <?= Html::a('Actualizar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Eliminar', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => '¿Desea Eliminar este elemento?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <?php
            $gridColumn = [
                ['attribute' => 'id', 'visible' => false],
                'name',
                'code',
                ['attribute' => 'photo_1', 'format' => 'html', 'value' => function ($model) {
                    return $model != null ? Html::img($model->photo_1, ['width' => 200, 'height' => 200]) : Yii::$app->params['not_found_image_url'];
                }],
                ['attribute' => 'photo_2', 'format' => 'html', 'value' => function ($model) {
                    return $model != null ? Html::img($model->photo_2, ['width' => 200, 'height' => 200]) : Yii::$app->params['not_found_image_url'];
                }],
                ['attribute' => 'photo_3', 'format' => 'html', 'value' => function ($model) {
                    return $model != null ? Html::img($model->photo_3, ['width' => 200, 'height' => 200]) : Yii::$app->params['not_found_image_url'];
                }],
                ['attribute' => 'photo_4', 'format' => 'html', 'value' => function ($model) {
                    return $model != null ? Html::img($model->photo_4, ['width' => 200, 'height' => 200]) : Yii::$app->params['not_found_image_url'];
                }],
                ['attribute' => 'photo_5', 'format' => 'html', 'value' => function ($model) {
                    return $model != null ? Html::img($model->photo_5, ['width' => 200, 'height' => 200]) : Yii::$app->params['not_found_image_url'];
                }],
                'county',
                'neighbor',
                'street',
                'lat',
                'long',
            ];
            echo DetailView::widget([
                'model' => $model,
                'attributes' => $gridColumn
            ]);
            $coord = new LatLng(['lat' => $model->lat, 'lng' => $model->long]);
            $map = new Map([
                'center' => $coord,
                'zoom' => 8,
                'width' => 'auto'
            ]);
            // Lets add a marker now
            $marker = new Marker([
                'position' => $coord,
                'draggable' => false,
                'title' => $this->title,
            ]);
            // Provide a shared InfoWindow to the marker
            // Add marker to the map
            $map->addOverlay($marker);
            echo $map->display();
            ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12"></div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <?php
            if ($providerFeature->totalCount) {
                $gridColumnFeature = [
                    ['class' => 'yii\grid\SerialColumn'],
                    ['attribute' => 'id', 'visible' => false],
                    'name',
                    'value',
                ];
                echo Gridview::widget([
                    'dataProvider' => $providerFeature,
                    'pjax' => true,
                    'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-feature']],
                    'panel' => [
                        'type' => GridView::TYPE_PRIMARY,
                        'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Características'),
                    ],
                    'columns' => $gridColumnFeature
                ]);
            }
            ?>
        </div>
    </div>
</div>
