<?php

namespace app\modules\construcciones\components;

use app\modules\construcciones\models\Construction;
use FoursquareApi;
use Yii;
use yii\base\Component;
use yii\helpers\Json;

/**
 * ConstructionComponent implements all the functionality and business layer of the {{%construction}} table.
 */
class ConstructionComponent extends Component
{
    /**
     * Get the images from the Forsquare API
     * @param Construction $construction
     * @return Construction
     */
    public function beforeSave($construction)
    {
        if (!$construction->isNewRecord) {
            return $construction;
        }
        $photosFromApi = $this->getPicturesFromForsquareApi($construction->county);
        \Yii::debug($photosFromApi);
        if (is_array($photosFromApi) && count($photosFromApi) >= 4) {
            $construction->photo_1 = $photosFromApi[0];
            $construction->photo_2 = $photosFromApi[1];
            $construction->photo_3 = $photosFromApi[2];
            $construction->photo_4 = $photosFromApi[3];
            $construction->photo_5 = $photosFromApi[4];
        }
        return $construction;
    }

    /**
     * @param Construction $construction
     */
    public function afterSave($construction)
    {

    }

    /**
     * @param $place
     * @return array|null
     */
    public function getPicturesFromForsquareApi($place): ?array
    {
        try {
            $foursquare = new FoursquareApi(Yii::$app->params['fq_client_id'], Yii::$app->params['fq_secret']);
            $endpoint = 'venues/search';
            $params = ['near' => $place];
            $venues = $foursquare->GetPublic($endpoint, $params, $POST = false);
            Yii::debug(Json::encode($venues));
            $venuesDecoded = Json::decode($venues);
            $venuesEncoded = Json::encode($venuesDecoded['response']['venues']);
            $results = [];
            foreach (Json::decode($venuesEncoded) as $key => $venue) {
                $endpoint = "venues/{$venue['id']}/photos";
                $params = [];
                $response = $foursquare->GetPublic($endpoint, $params);
                $response = Json::decode($response);
                Yii::debug(Json::encode($response));
                $photos = Json::encode($response['response']['photos']['items']);
                foreach (Json::decode($photos) as $innerKey => $photo) {
                    $results[] = "{$photo['prefix']}{$photo['width']}x{$photo['height']}{$photo['suffix']}";
                }
            }
            return $results;
        } catch (\Exception $exception) {
            return ['error' => $exception->getMessage()];
        }
    }
}
