<?php

namespace app\modules\construcciones\components;

use inquid\date_time\DateTimeHandler;
use inquid\pdf\FPDF;
use Yii;
use app\modules\construcciones\models\Construction;

/**
* ConstructionPDF implements the PDF printable view for Construction model.
*/
class ConstructionPDF extends FPDF
{
    /** @var array $color */
    private $color = ['5', '100', '36'];
public $Construction;
	public function Header(){
        		$this->SetTitle('Construcciones-' . $this->Construction->id);
        		$this->SetFillColor($this->color[0], $this->color[1], $this->color[2]);
        		$this->SetFont('Arial', 'B', 12);
        		$this->Cell(40, 4, '', 0, 0, 'C');
        		$this->Cell(105, 4, utf8_decode('Título ...'), 0, 0, 'C');
	}
	public function Body(){
        		$this->AliasNbPages();
        		$this->AddPage();
        		$this->Ln(4);
        		$this->SetFillColor(		$this->color[0], 		$this->color[1], 		$this->color[2]);
        		$this->SetFont('Arial', 'B', 8);
        		$this->SetTextColor(255, 255, 255);
        		$this->Ln(2);   
	}
	public function Footer(){
                         // Position at 1.5 cm from bottom
        		$this->SetY(-40);
        		$this->SetFont('Arial', 'B', 8);
	}
    /**
     * @return string
     */
    public function saveToFile()
    {
        $this->Output('F', \Yii::getAlias('@app/web/files/PurchaseOrder/construction' . DateTimeHandler::getDateTime('Y-m-d') . '.pdf'));
        return \Yii::getAlias('@app/web/files/PurchaseOrder/construction-' . DateTimeHandler::getDateTime('Y-m-d') . '.pdf');
    }
}
