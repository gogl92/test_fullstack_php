<?php

namespace app\modules\construcciones\components;
use Yii;
use app\modules\construcciones\models\Feature;
use yii\base\Component;

/**
* FeatureComponent implements all the functionality and business layer of the {{%feature}} table.
*/
class FeatureComponent extends Component
{
	public function beforeSave(){

	}

	public function afterSave(){

	}
}
