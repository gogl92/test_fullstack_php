<?php

namespace app\modules\construcciones\models\base;

use mootensai\relation\RelationTrait;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the base model class for table "{{%construction}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $code
 * @property string $photo_1
 * @property string $photo_2
 * @property string $photo_3
 * @property string $photo_4
 * @property string $photo_5
 * @property string $county
 * @property string $neighbor
 * @property string $street
 * @property string $lat
 * @property string $long
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $deleted_by
 *
 * @property \app\modules\construcciones\models\Feature[] $features
 */
class Construction extends ActiveRecord
{
    use RelationTrait;

    private $_rt_softdelete;
    private $_rt_softrestore;

    public function __construct()
    {
        parent::__construct();
        $this->_rt_softdelete = [
            'deleted_by' => \Yii::$app->user->id,
            'deleted_at' => date('Y-m-d H:i:s'),
        ];
        $this->_rt_softrestore = [
            'deleted_by' => 0,
            'deleted_at' => date('Y-m-d H:i:s'),
        ];
    }

    /**
     * This function helps \mootensai\relation\RelationTrait runs faster
     * @return array relation names of this model
     */
    public function relationNames()
    {
        return [
            'features'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'code', 'street', 'lat', 'long', 'county'], 'required'],
            [['created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['name', 'county', 'neighbor', 'street', 'created_at', 'updated_at', 'deleted_at'], 'string', 'max' => 45],
            [['code'], 'string', 'max' => 15],
            [['photo_1', 'photo_2', 'photo_3', 'photo_4', 'photo_5'], 'string', 'max' => 200],
            [['lat', 'long'], 'string', 'max' => 20],
            ['name', 'match', 'pattern' => '/^[a-zA-Z]+$/'],
            ['code', 'match', 'pattern' => '/PCOM-[0-9][0-9][0-9]\/[0-9][0-9]/'],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%construction}}';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Nombre',
            'code' => 'Clave',
            'photo_1' => 'Foto 1',
            'photo_2' => 'Foto 2',
            'photo_3' => 'Foto 3',
            'photo_4' => 'Foto 4',
            'photo_5' => 'Foto 5',
            'county' => 'Delegación',
            'neighbor' => 'Colonia',
            'street' => 'Calle',
            'lat' => 'Latitud',
            'long' => 'Longitud',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFeatures()
    {
        return $this->hasMany(\app\modules\construcciones\models\Feature::className(), ['construction_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('CONVERT_TZ(NOW(),"+00:00","-05:00")'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }

    /**
     * The following code shows how to apply a default condition for all queries:
     *
     * ```php
     * class Customer extends ActiveRecord
     * {
     *     public static function find()
     *     {
     *         return parent::find()->where(['deleted' => false]);
     *     }
     * }
     *
     * // Use andWhere()/orWhere() to apply the default condition
     * // SELECT FROM customer WHERE `deleted`=:deleted AND age>30
     * $customers = Customer::find()->andWhere('age>30')->all();
     *
     * // Use where() to ignore the default condition
     * // SELECT FROM customer WHERE age>30
     * $customers = Customer::find()->where('age>30')->all();
     * ```
     */

    /**
     * @inheritdoc
     * @return \app\modules\construcciones\models\ActiveQuery\ConstructionQuery the active query used by this AR class.
     */
    public static function find()
    {
        $query = new \app\modules\construcciones\models\ActiveQuery\ConstructionQuery(get_called_class());
        return $query->where(['construction.deleted_by' => 0]);
    }
}
