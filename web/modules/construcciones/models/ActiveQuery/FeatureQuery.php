<?php

namespace app\modules\construcciones\models\ActiveQuery;

/**
 * This is the ActiveQuery class for [[\app\modules\construcciones\models\ActiveQuery\Feature]].
 *
 * @see \app\modules\construcciones\models\ActiveQuery\Feature
 */
class FeatureQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \app\modules\construcciones\models\ActiveQuery\FeatureQuery[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \app\modules\construcciones\models\ActiveQuery\FeatureQuery|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
