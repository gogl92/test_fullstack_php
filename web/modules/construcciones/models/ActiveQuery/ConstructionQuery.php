<?php

namespace app\modules\construcciones\models\ActiveQuery;

/**
 * This is the ActiveQuery class for [[\app\modules\construcciones\models\ActiveQuery\Construction]].
 *
 * @see \app\modules\construcciones\models\ActiveQuery\Construction
 */
class ConstructionQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \app\modules\construcciones\models\ActiveQuery\ConstructionQuery[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \app\modules\construcciones\models\ActiveQuery\ConstructionQuery|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
