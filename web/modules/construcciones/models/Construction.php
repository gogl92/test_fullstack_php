<?php

namespace app\modules\construcciones\models;

use app\modules\construcciones\components\ConstructionComponent;
use app\modules\construcciones\models\base\Construction as BaseConstruction;

/**
 * This is the model class for table "construction".
 */
class Construction extends BaseConstruction
{
    public $fileExcelImport;

    public function beforeSave($insert)
    {
        $component = new ConstructionComponent();
        $this->attributes = $component->beforeSave($this);
        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {
        $component = new ConstructionComponent();
        $component->afterSave($this);
        parent::afterSave($insert, $changedAttributes);
    }
}
