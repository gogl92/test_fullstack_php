<?php

namespace app\modules\construcciones\models;

use Yii;
use \app\modules\construcciones\models\base\Feature as BaseFeature;

/**
* This is the model class for table "feature".
*/
class Feature extends BaseFeature
{
    public $fileExcelImport;

    public function beforeSave($insert)
    {
        return parent::beforeSave($insert); // TODO: Change the autogenerated stub
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes); // TODO: Change the autogenerated stub
    }
}
