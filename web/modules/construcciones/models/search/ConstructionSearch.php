<?php

namespace app\modules\construcciones\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\construcciones\models\Construction;

/**
 * app\modules\construcciones\models\search\ConstructionSearch represents the model behind the search form about `app\modules\construcciones\models\Construction`.
 */
 class ConstructionSearch extends Construction
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['name', 'code', 'photo_1', 'photo_2', 'photo_3', 'photo_4', 'photo_5', 'county', 'neighbor', 'street', 'lat', 'long', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Construction::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $query->orderBy(['id' => SORT_DESC]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
            'deleted_by' => $this->deleted_by,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'code', $this->code])
            ->andFilterWhere(['like', 'photo_1', $this->photo_1])
            ->andFilterWhere(['like', 'photo_2', $this->photo_2])
            ->andFilterWhere(['like', 'photo_3', $this->photo_3])
            ->andFilterWhere(['like', 'photo_4', $this->photo_4])
            ->andFilterWhere(['like', 'photo_5', $this->photo_5])
            ->andFilterWhere(['like', 'county', $this->county])
            ->andFilterWhere(['like', 'neighbor', $this->neighbor])
            ->andFilterWhere(['like', 'street', $this->street])
            ->andFilterWhere(['like', 'lat', $this->lat])
            ->andFilterWhere(['like', 'long', $this->long])
            ->andFilterWhere(['like', 'created_at', $this->created_at])
            ->andFilterWhere(['like', 'updated_at', $this->updated_at])
            ->andFilterWhere(['like', 'deleted_at', $this->deleted_at]);

        return $dataProvider;
    }
}
