<?php

namespace app\modules\construcciones;

use Yii;

/**
 * construcciones module definition class
 */
class construcciones extends \yii\base\Module
{
    public $menu = [];

    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\construcciones\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
        //Yii::configure($this, require(__DIR__ . '/config/config.php'));
        if (isset(Yii::$app->user->identity)) {
            $this->menu = [
                'label' => 'CRUD',
                'visible' => !Yii::$app->user->isGuest,
                'url' => ['/construcciones/default/index'],
                'template' => '<a href="{url}">{label}<i class="fa fa-angle-left pull-right"></i></a>',
                'items' => [
                    ['label' => 'Construcciones', 'url' => ['/construcciones/construction/index'], 'visible' => true],
                    ['label' => 'Características', 'url' => ['/construcciones/feature/index'], 'visible' => true],
                ],
            ];
        }
        // custom initialization code goes here
    }
}
