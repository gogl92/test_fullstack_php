<?php

use yii\db\Schema;

class m190311_030101_construction extends \yii\db\Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
        
        $this->createTable('construction', [
            'id' => $this->primaryKey(),
            'name' => $this->string(45)->notNull(),
            'code' => $this->string(15)->notNull(),
            'photo_1' => $this->string(200),
            'photo_2' => $this->string(200),
            'photo_3' => $this->string(200),
            'photo_4' => $this->string(200),
            'photo_5' => $this->string(200),
            'county' => $this->string(45),
            'neighbor' => $this->string(45),
            'street' => $this->string(45),
            'lat' => $this->string(20),
            'long' => $this->string(20),
            'created_at' => $this->string(45),
            'updated_at' => $this->string(45),
            'deleted_at' => $this->string(45),
            'created_by' => $this->integer(11),
            'updated_by' => $this->integer(11),
            'deleted_by' => $this->integer(11)->defaultValue(0),
            ], $tableOptions);
                
    }

    public function down()
    {
        $this->dropTable('construction');
    }
}
