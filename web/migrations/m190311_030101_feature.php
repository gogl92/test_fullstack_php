<?php

use yii\db\Schema;

class m190311_030101_feature extends \yii\db\Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
        
        $this->createTable('feature', [
            'id' => $this->primaryKey(),
            'name' => $this->string(45)->notNull(),
            'value' => $this->string(200)->notNull(),
            'construction_id' => $this->integer(11)->notNull(),
            'created_at' => $this->string(45),
            'updated_at' => $this->string(45),
            'deleted_at' => $this->string(45),
            'created_by' => $this->integer(11),
            'updated_by' => $this->integer(11),
            'deleted_by' => $this->integer(11)->defaultValue(0),
            'FOREIGN KEY ([[construction_id]]) REFERENCES construction ([[id]]) ON DELETE CASCADE ON UPDATE CASCADE',
            ], $tableOptions);
                
    }

    public function down()
    {
        $this->dropTable('feature');
    }
}
