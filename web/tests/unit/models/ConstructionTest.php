<?php
/**
 * Created by PhpStorm.
 * User: gogl92
 * Date: 2019-03-11
 * Time: 00:43
 */

namespace tests\unit\models;


use app\modules\construcciones\models\Construction;
use Faker\Factory;
use yii\helpers\Json;


class ConstructionTest extends \Codeception\Test\Unit
{
    public $id = 1;
    public function testCreate()
    {
        $faker = Factory::create();
        $construction = new Construction();
        $construction->name = $faker->text(10);
        $construction->code = 'PCOM-001/19';
        $construction->lat = $faker->latitude;
        $construction->long = $faker->longitude;
        $construction->county = $faker->city;
        $construction->save();
        expect_that(empty($construction->getErrors()));
    }

    public function testUpdate()
    {
        $faker = Factory::create();
        $construction = new Construction();
        $construction->name = $faker->text(10);
        $construction->code = 'PCOM-001/19';
        $construction->lat = $faker->latitude;
        $construction->long = $faker->longitude;
        $construction->county = $faker->city;
        $construction->save();
        expect_that(empty($construction->getErrors()));
    }

    public function testView()
    {
        $id = $this->id;
        $construction = \app\modules\construcciones\models\base\Construction::find()->where(['id' => $id])->one();
        expect_that($construction !== null);
    }

    public function testList()
    {
        expect_not(empty(Construction::find()->all()));
    }

    public function testDelete()
    {
        $id = $this->id;
        $construction = Construction::find()->where(['id' => $id])->one();
        expect_that($construction->delete());
    }

}