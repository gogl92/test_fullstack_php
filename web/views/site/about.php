<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Acerca del Proyecto (Requerimientos)';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>
    <article class="sc-hAXbOi koJJse" data-qa="readme-container"><h1
                id="markdown-header-examen-practico-software-engineer">Examen Práctico: Software Engineer</h1>
        <p>La inmobiliaria <strong>Propiedades</strong> requiere de una aplicación web para la administración de sus
            construcciones con las siguientes consideraciones:</p>
        <ul>
            <li>Permitir el registro y autenticación de usuarios</li>
            <li>El <strong>CRUD</strong> de las construcciones
                <ul>
                    <li>Puedes utilizar un framework de tu preferencia con arquitectura <strong>MVC</strong></li>
                    <li>El formulario de registro para las construcciones debe contar con las siguientes restricciones:
                        <ul>
                            <li>Nombre de la construcción: validar solo el uso de textos</li>
                            <li>Clave de la construcción: respetar el siguiente formato <strong>PCOM-XXX/##</strong>
                            </li>
                            <li>Galeria de imagenes: deberá consumir el <strong>API de Foursquare</strong> (en php) para
                                obtener 5 fotos de localidades cercanas a la ubicación de la construcción y almacenarlas
                                como parte de su la galería
                            </li>
                        </ul>
                    </li>
                    <li>Datos de ubicación mínimos requeridos: Delegación, Colonia, calle</li>
                    <li>Datos de geoposicionamiento: latitud y longitud</li>
                    <li>Características: Se puede agregar N características de forma dinámica</li>
                </ul>
            </li>
            <li>Crea un mapa con el <strong>API de google maps</strong> donde se visualicen los pins de las
                construcciones registradas
                <ul>
                    <li>Al seleccionar cualquier pin del mapa; debe mostrar el nombre de la construcción y desplegar una
                        galería responsive de sus fotos almacenadas
                    </li>
                    <li>Al terminar de cargar la galeria; agregar un link que permita mostrar los detalles de
                        construction (Nombre, Clave, Caracteristicas, etc.)
                    </li>
                </ul>
            </li>
        </ul>
        <hr>
        <h2 id="markdown-header-requerimientos">Requerimientos:</h2>
        <ul>
            <li>Configura un servidor <strong>LAMP</strong> de forma local para verificar el funcionamiento del
                ejercicio.
            </li>
            <li>Base de datos: Se adjuntan sentencias sql para la configuración inicial.</li>
        </ul>
        <div class="codehilite"><pre><span></span>CREATE DATABASE Testing_fullstack;
USE Testing_fullstack;
CREATE TABLE Users (
    ID int NOT NULL AUTO_INCREMENT,
    FirstName varchar(255) NOT NULL,
    LastName varchar(255),
    Email varchar(100),
    Password varchar(255),
    PRIMARY KEY (ID)
);
INSERT INTO Users (FirstName,LastName, Email, Password) VALUES ('Nathan', 'Smith', 'pcom@gmail.com', '3dd14afc9f2da6c03c4f6599553a4597');
</pre>
        </div>


        <blockquote>
            <p><strong>Nota: </strong> Estas sentencias las puede encontrar en el archivo <strong>query.sql</strong> de
                la carpeta <strong>bd</strong></p>
        </blockquote>
        <ul>
            <li>Repositorio:
                <ul>
                    <li>Debe contar con una cuenta en <strong>bitbucket</strong> para poder bifurcar
                        (<strong>fork</strong>) el repositorio
                    </li>
                    <li>Realice el examen práctico sobre la copia que realizó</li>
                    <li>Al finalizar la prueba, enviar el link de su repositorio al siguiente correo: <strong>luis.castro@propiedades.com</strong>
                    </li>
                </ul>
            </li>
        </ul>
        <h2 id="markdown-header-especificaciones">Especificaciones:</h2>
        <ul>
            <li>Tiene la libertad de modelar la estructura de la <strong>BD</strong> de acuerdo a sus necesidades,
                respetando los <strong>query</strong> que se proporciona.
            </li>
            <li>El uso de los commit debe contar con una descripción detallada.</li>
            <li>Al finalizar la prueba, en la carpeta <strong>BD</strong> crear el archivo <strong>dump.sql</strong> de
                la estructura final de la base de datos que requiere
            </li>
            <li>Estructura del repositorio:</li>
        </ul>
        <dl>
            <dt>bd</dt>
            <dd>query.sql</dd>
            <dt>web</dt>
            <dd>index.php</dd>
        </dl>
        <p><strong>config.txt</strong></p>
        <ul>
            <li>En el archivo <strong>config.txt</strong> especificar la configuración que su examen requiere para que
                pueda funcionar correctamente. Si realizo el uso de algún <strong>framework</strong>, favor de
                especificar el nombre y la versión para su adecuado funcionamiento.
            </li>
        </ul>
    </article>
</div>
