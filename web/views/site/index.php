<?php

/* @var $this yii\web\View */

$this->title = Yii::$app->name;

use yii\helpers\Html; ?>
<div class="site-index">

    <div class="jumbotron">
        <?= Html::img(Yii::getAlias('@web/images/logo.svg')) ?>

        <p class="lead">Examen Práctico: Software Engineer</p>

        <p><a class="btn btn-lg btn-success" href="http://www.yiiframework.com">Powered by Yii</a></p>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <h2>Propiedades</h2>

                <p>Administra las construcciones disponibles en la empresa.</p>

                <p><a class="btn btn-default" href="/construcciones/construction/index">Administrar &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Características</h2>

                <p>Administra las características de las construcciones.</p>

                <p><a class="btn btn-default" href="/construcciones/feature/index">Administrar &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Documentación</h2>

                <p>Ver documentación del proyecto.</p>

                <p><a class="btn btn-default" href="<?=Yii::getAlias('@web/docs/guide-config.html')?>">Ver documentación &raquo;</a></p>
            </div>
        </div>

    </div>
</div>
