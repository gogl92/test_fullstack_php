# Configuración del examen practico.

REQUIREMENTS
------------
* The minimum requirement by this project template that your Web server supports PHP 7.1.0.
* The framework used is Yii2 version 2.0.16
* This project was tested in MySQL server version 5.7 Google version

INSTALLATION
------------
To install this project please follow the next steps:

* Clone the repository: git clone git@bitbucket.org:gogl92/test_fullstack_php.git
* Change directory to prooject root/web
* execute ``` composer install ```
* Configure the database in config/db.php
* execute the following migrations to crete the database tables: (Recommended method)
    ```
        ./yii migrate --migrationNamespaces=Da\\User\\Migration
        ./yii migrate --migrationPath=@yii/rbac/migrations
        ./yii migrate 
     ```
* You can also use the backup script int the bd or the MySQL Workbench model     
* Point your server (Apache, Nginx, PHP built in) to the web folder inside the Yii2 project

USE
----
Open your server and go to the registration option in the menu
![home](web/web/images/home.png "Home")

Registration Tip: create first the admin user which it username is "admin"
![home](web/web/images/register.png "Register")

After creating your user you can use the CRUD
![home](web/web/images/construcciones.png "Construcciones")

Create a Construction Remember to drag the marker to the exact location, also remember that the "delegación" field will be used to get the images from forsquare
![home](web/web/images/form.png "Form")
Add many "Características" as you want
![home](web/web/images/bottom_form.png "Bottom Form")

To see all the points in a interactive map go to the "construcciones" index and then in the option "Vista General en mapa"
![home](web/web/images/map.png "Mapa")
At the bottom of the globe you will see a link to the details of the construcion